local UserInputService = game:GetService("UserInputService")
local RunService = game:GetService("RunService")
local Players = game:GetService("Players")

getgenv().Multiplier = 0.1

local function applySpeedBoost(character)
    while character do
        local humanoidRootPart = character:FindFirstChild("HumanoidRootPart")
        local humanoid = character:FindFirstChildOfClass("Humanoid")

        if humanoidRootPart and humanoid then
            local moveDirection = humanoid.MoveDirection
            humanoidRootPart.CFrame = humanoidRootPart.CFrame + moveDirection * getgenv().Multiplier
        end
        
        RunService.Stepped:Wait()
    end
end

Players.LocalPlayer.CharacterAdded:Connect(function(character)
    applySpeedBoost(character)
end)

local existingCharacter = Players.LocalPlayer.Character
if existingCharacter then
    applySpeedBoost(existingCharacter)
end