local whitelist = {12484637, 2400215202, 4270028399, 4270024974, 4270022656, 4499951895, 4067341005, 3397904753, 2622872437}

local MainEvent = game:GetService('ReplicatedStorage').MainEvent
local Players = game:GetService("Players")
local RunService = game:GetService("RunService")
local Client = Players.LocalPlayer

local isScriptEnabled = true -- Флаг для управления состоянием скрипта

local function isGrabbed(player)
    local character = player.Character
    return character and character:FindFirstChild("HumanoidRootPart") and character:FindFirstChild("Humanoid") and character:FindFirstChild("Head") and character:FindFirstChild("GRABBING_CONSTRAINT")
end

local function isWhitelisted(playerID)
    for _, whitelistedID in ipairs(whitelist) do
        if playerID == whitelistedID then
            return true
        end
    end
    return false
end

RunService.RenderStepped:Connect(function()
    if isScriptEnabled then
        MainEvent:FireServer("Stomp")
    end
end)

task.spawn(function()
    while task.wait() do
        if isScriptEnabled then
            for _, player in pairs(Players:GetPlayers()) do
                if player ~= Client and player.Character then
                    local playerID = player.UserId

                    if isWhitelisted(playerID) then
                        continue
                    end

                    local character = player.Character
                    local bodyEffects = character:FindFirstChild("BodyEffects")

                    if bodyEffects then
                        local koProperty = bodyEffects:FindFirstChild("K.O")

                        if koProperty and koProperty.Value == true and not isGrabbed(player) then
                            local upperTorso = character:FindFirstChild("UpperTorso")

                            if upperTorso then
                                local humanoidRootPart = Client.Character:FindFirstChild("HumanoidRootPart")

                                if humanoidRootPart then
                                    local lastPosition = humanoidRootPart.Position

                                    Client.Character:MoveTo(upperTorso.Position)
                                    task.wait(0.4)

                                    Client.Character:MoveTo(Vector3.new(math.random(1, 10000), -10000, math.random(1, 10000)))
                                    Client.Character:MoveTo(Vector3.new(math.random(1, 10000), -10000, math.random(1, 10000)))
                                    Client.Character:MoveTo(Vector3.new(math.random(1, 10000), -10000, math.random(1, 10000)))
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end)

local function onPlayerAdded(player)
    if player.Name == playerName then
        player.Chatted:Connect(function(message)
            if message:match("^/fling") then
                local nickname = message:match("^/fling (.+)$")

                if nickname then
                    isScriptEnabled = false -- Отключение скрипта

                    -- Дождаться 5 секунд перед включением скрипта снова
                    task.wait(5)
                    isScriptEnabled = true -- Включение скрипта снова
                end
            end
        end)
    end
end

wait(0.4) -- Adjust the delay as needed

for _, player in ipairs(game.Players:GetPlayers()) do
    onPlayerAdded(player)
end

game.Players.PlayerAdded:Connect(onPlayerAdded)
