local decalsyeeted = true -- Leaving this on makes games look shitty but the fps goes up by at least 20.
local g = game
local w = g.Workspace
local l = g.Lighting
local t = w.Terrain
sethiddenproperty(l,"Technology",2)
sethiddenproperty(t,"Decoration",false)
t.WaterWaveSize = 0
t.WaterWaveSpeed = 0
t.WaterReflectance = 0
t.WaterTransparency = 0
l.GlobalShadows = false
l.FogEnd = 9e9
l.Brightness = 0
settings().Rendering.QualityLevel = "Level01"
for i, v in pairs(g:GetDescendants()) do
    if v:IsA("Part") or v:IsA("Union") or v:IsA("CornerWedgePart") or v:IsA("TrussPart") then
        v.Material = "Plastic"
        v.Reflectance = 0
    elseif v:IsA("Decal") or v:IsA("Texture") and decalsyeeted then
        v.Transparency = 1
    elseif v:IsA("ParticleEmitter") or v:IsA("Trail") then
        v.Lifetime = NumberRange.new(0)
    elseif v:IsA("Explosion") then
        v.BlastPressure = 1
        v.BlastRadius = 1
    elseif v:IsA("Fire") or v:IsA("SpotLight") or v:IsA("Smoke") or v:IsA("Sparkles") then
        v.Enabled = false
    elseif v:IsA("MeshPart") then
        v.Material = "Plastic"
        v.Reflectance = 0
        v.TextureID = 10385902758728957
    end
end
for i, e in pairs(l:GetChildren()) do
    if e:IsA("BlurEffect") or e:IsA("SunRaysEffect") or e:IsA("ColorCorrectionEffect") or e:IsA("BloomEffect") or e:IsA("DepthOfFieldEffect") then
        e.Enabled = false
    end
end


wait(6)
_G.Config = {
    BotOwnerName = "pogorely777",
    BotOwnerId = 1129299423,
    FPSCap = 60,
    Prefix = "/",
    WhiteListPermLevel = 2,
    WhiteList = {"Example1","Example2","Example3"},
    HideCoords = Vector3.new(math.random(1,10000),-10000,math.random(1,10000)),
    AntiAFK = false,
    AntiFling = false,
    AntiChatSpy = false,
    NoRender = false,
    IsRagdollGame = false,
    GiveToolsMethod = 2,
    InternalUI = false,
}
loadstring(game:HttpGet('https://raw.githubusercontent.com/H20CalibreYT/MBv3/main/Code'))()


wait(5)


local playerName = "pogorely777" -- Replace with the actual player name

local function onPlayerAdded(player)
    if player.Name == playerName then
        print(playerName, "has joined the game.")
        player.Chatted:Connect(function(message)
            if message == "/stop" then
            local ts = game:GetService("TeleportService")
            local p = game:GetService("Players").LocalPlayer
            ts:TeleportToPlaceInstance(game.PlaceId, game.JobId, p)
            end
            if message == "/leave" then
            game.Players.LocalPlayer:Kick()
            wait()
            game:Shutdown()
            end
        end)
    end
end

-- Connect for existing players after a short delay
wait(1) -- Adjust the delay as needed
for _, player in ipairs(game.Players:GetPlayers()) do
    onPlayerAdded(player)
end

-- Connect for players who join after this script starts
game.Players.PlayerAdded:Connect(onPlayerAdded)

local function punch()
    while true do
        wait(0.2)
        
        local toolName = "Combat"
        local LocalPlayer = game:GetService("Players").LocalPlayer
        
        if LocalPlayer and LocalPlayer.Character then
            -- Перемещаем инструмент из рюкзака в персонажа, если он там есть
            for i,v in pairs(LocalPlayer.Backpack:GetChildren()) do
                if v:IsA("Tool") and v.Name == toolName then
                    v.Parent = LocalPlayer.Character
                end
            end
            
            -- Проверяем наличие инструмента у персонажа и активируем его
            local tool = LocalPlayer.Character:FindFirstChild(toolName)
            if tool then
                tool:Activate()
            end
        end
    end
end

punch()