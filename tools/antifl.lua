local RunService = game:GetService("RunService")
local players = game:GetService("Players")
local plr = players.LocalPlayer
local AntiFlingFunction = nil
local antiFlingToggled = false  -- Добавьте флаг

-- Функция для отключения флинга
function disableFling(character)
    for _, v in pairs(character:GetChildren()) do
        if v:IsA("BasePart") or (v:IsA("Accessory") and v:FindFirstChild("Handle")) then
            local part = v:IsA("BasePart") and v or v.Handle
            part.CanCollide = false
            part.Velocity = Vector3.new(0,0,0)
            part.RotVelocity = Vector3.new(0,0,0)
            part.CustomPhysicalProperties = PhysicalProperties.new(0,0,0,0,0)
            part.Massless = true
        end
    end
end

-- Подключение функции, если переключатель активен
AntiFlingFunction = RunService.Stepped:Connect(function()
    if not antiFlingToggled then return end  -- Проверка флага
    for _, player in ipairs(players:GetPlayers()) do
        if player ~= plr and player.Character then
            disableFling(player.Character)
        end
    end
end)

-- Добавьте команду или событие, чтобы установить переключатель
_G.AntiFlingToggled = function(state)
    antiFlingToggled = state
    if not state then
        AntiFlingFunction:Disconnect()
    end
end
