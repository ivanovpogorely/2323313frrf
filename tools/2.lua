local 23Asmo = getrawmetatable(game)
setreadonly(23Asmo , false)
local old23Asmo  = 23Asmo .__namecall
23Asmo .__namecall = newcclosure(function(self, ...)
    local method = getnamecallmethod()
    if method == "IsInGroup" then
        return true
    end
    return old23Asmo(self, ...)
end)
setreadonly(23Asmo, true)

getgenv().settings =  {
  ['Stand'] =4006120431, --userid of the stand
  ['Owner'] =12484637, -- userid of the jojo stand owner
}
getgenv().commands =  {
   ['Summon'] = "Summon!", -- if someone else wants to own the stand.
   ['Follow'] = "Follow!", -- follow/unfollow you
   ['Ghost'] = "Disappear!", -- disappear
   ['Unghost'] = "Appear!", -- appear
   ['Attack'] = "Ora!", -- loop attack a target
   ['Stopattack'] = "Booga!", -- stop attacking the target
   ['Void'] = "Void!", -- send the target to the void when ko (goes with attack)
   ['Godmode'] = "Requiem!", -- enable/disable godmode
   ['Autosave'] = "Save!", -- grab you when k.o
   ['Autosave2'] = "Bird!", -- send you in the air when k.o
   ['Reset'] = "Stop!", -- resets commands and character (debug)
}
 
loadstring(game:HttpGet("https://raw.githubusercontent.com/racemodex/my-scripts/master/dahoodjojostand-v3rmversion", true))()
